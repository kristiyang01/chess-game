#include "Bishop.h"


Bishop::Bishop(Position position, Color color, bool hasMoved, const string& type) : Piece(position, color, hasMoved, type) {}

std::vector<Position> Bishop::getMovementDirection() {

    std::vector<Position> movementDirection;   
    
    movementDirection.push_back({ 1, 1 });
    movementDirection.push_back({ 1, -1 });
    movementDirection.push_back({ -1, 1 });
    movementDirection.push_back({ -1, -1 });

    return movementDirection;


}

short Bishop::getMovementRange() {
    return 7;
}

std::vector<Position> Bishop::getSpecialMovementDirection() {
    return {};
}

short Bishop::getSpecialMovementRange() {
    return 0;
}