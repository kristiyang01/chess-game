#ifndef BISHOP_H
#define BISHOP_H

#include "Piece.h"

class Bishop : public Piece
{

public:

    ~Bishop() = default;
    Bishop(Position position, Color color, bool hasMoved, const string& type);

    std::vector<Position> getMovementDirection() override;
    short getMovementRange() override;
    std::vector<Position> getSpecialMovementDirection() override;
    short getSpecialMovementRange() override;
};



#endif