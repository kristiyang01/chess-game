#include <iostream>
#include "Board.h"

Board::Board() {
    initializeSquares(); 
    initializePieces();
}

void Board::setPieceToSquare(short row, short column, Piece* piece) {
    squares[row][column].setPiece(piece);
}

void Board::setSquare(short row, short column, Piece* piece, const Color& color)
{
    squares[row][column].setPiece(piece);
    squares[row][column].setColor(color);
}

const Square& Board::getSquare(short row, short column) const {
    return squares[row][column];
}

Square* Board::getSquare(short row, short column) {
    return &squares[row][column];
}

Piece* Board::getPieceAt(Position pos) {
    return squares[pos.row][pos.column].getPiece();
}

void Board::initializeSquares() {
    for (int row = 0; row < 8; row++) {
        for (int column = 0; column < 8; column++) {
            if (row % 2 == column % 2) {
                squares[row][column].setColor(Color::WHITE);
            }
            else {
                squares[row][column].setColor(Color::BLACK);
            }
        }
    }
}

void Board::initializePieces() {

    for (short i = 0; i < 8; i++) {
        setPieceToSquare(6, i, pieces[i]);
    }

    setPieceToSquare(7, 1, pieces[8]);
    setPieceToSquare(7, 6, pieces[9]);
    setPieceToSquare(7, 2, pieces[10]);
    setPieceToSquare(7, 5, pieces[11]);
    setPieceToSquare(7, 0, pieces[12]);
    setPieceToSquare(7, 7, pieces[13]);
    setPieceToSquare(7, 3, pieces[14]);
    setPieceToSquare(7, 4, pieces[15]);


    for (short i = 0; i < 8; i++) {
        setPieceToSquare(1, i, pieces[i + 16]);
    }

    setPieceToSquare(0, 1, pieces[24]);
    setPieceToSquare(0, 6, pieces[25]);
    setPieceToSquare(0, 2, pieces[26]);
    setPieceToSquare(0, 5, pieces[27]);
    setPieceToSquare(0, 0, pieces[28]);
    setPieceToSquare(0, 7, pieces[29]);
    setPieceToSquare(0, 3, pieces[30]);
    setPieceToSquare(0, 4, pieces[31]);
}

void Board::unhighlightSquares(std::vector<Position> possibleMoves, short possibleMovesCount) {
    for (int i = 0; i < possibleMovesCount; i++) {
        squares[possibleMoves[i].row][possibleMoves[i].column].setHighlighted(false);
    }
}
void Board::highlightSquares(std::vector<Position> possibleMoves, short possibleMovesCount) { //TO DO maybe name should be markSquareForHighlighting
    for (int i = 0; i < possibleMovesCount; i++) {
        squares[possibleMoves[i].row][possibleMoves[i].column].setHighlighted(true); //TO DO maybe name should be markForHighlighting
    }
}

void Board::makeMove(Square* initialSquare, Square* targetSquare, short row, short column) {
    Piece* piece = initialSquare->getPiece();
    piece->setPosition(row, column);
    targetSquare->setPiece(piece);
    initialSquare->setPiece(nullptr);
    piece->setHasMoved(true);
}

std::vector<Position> Board::getPossiblePieceMoves(Position attackingPiecePosition) {  //TO DO refactor this for sure its too long
    std::vector<Position> possibleMoves; //TO DO maybe think if stack is good here?
    
    Square* attackingSquare = getSquare(attackingPiecePosition.row, attackingPiecePosition.column);
    Piece* attackingPiece = attackingSquare->getPiece();

    Position positionToCheck;

    if (attackingPiece->getType() == "Pawn") {
        
        short pieceMovementRange = attackingPiece->getSpecialMovementRange();
        std::vector<Position> movementDirections = attackingPiece->getSpecialMovementDirection();
        short size = movementDirections.size();

        for (int i = 0; i < size; i++) {
            positionToCheck.row = attackingPiecePosition.row + movementDirections[i].row;
            positionToCheck.column = attackingPiecePosition.column + movementDirections[i].column;
            Square* squareToCheck = getSquare(positionToCheck.row, positionToCheck.column);

            if (i == 0) {
                if (!squareToCheck->hasPiece()) {
                    possibleMoves.push_back(positionToCheck);

                    if (!attackingPiece->hasBeenMoved()) {
                        positionToCheck.row += movementDirections[i].row;
                        squareToCheck = getSquare(positionToCheck.row, positionToCheck.column);
                        if (!squareToCheck->hasPiece()) {
                            possibleMoves.push_back(positionToCheck); //TO DO REFACTOR THIS FOR SURE!!!!!!!
                        }
                    }
                }
            }
            else {
                if (squareToCheck->hasPiece() && squareToCheck->getPieceColor() != attackingPiece->getColor()) { //TO DO  || lastMoveLeadsToEnpassant  and maybe help function
                    possibleMoves.push_back(positionToCheck);
                }
            }
        }        
    } 
    else {
        short pieceMovementRange = attackingPiece->getMovementRange();
        std::vector<Position> movementDirections = attackingPiece->getMovementDirection();
        short size = movementDirections.size();

        /* delete thissssss + the whole function maybe.  after im finished with rulebook (DO)
        if (attackingPiece->getType() == "King") {
            if (leftSideCastleAllowed()) {
                specialMovementDirection = attackingSquare->getPieceSpecialMovementDirection();
                specialMovementRange = specialMovementDirection.size();
            }
        }*/

        for (int i = 0; i < size; i++) {
            positionToCheck = attackingPiecePosition;

            for (int j = 0; j < pieceMovementRange; j++) {

                positionToCheck.row += movementDirections[i].row;
                positionToCheck.column += movementDirections[i].column;
                if (!positionToCheck.isValidPosition()) {
                    break;
                }

                Square* squareToCheck = getSquare(positionToCheck.row, positionToCheck.column); 

                if (squareToCheck->hasPiece()) {
                    if (attackingSquare->getPieceColor() != squareToCheck->getPieceColor()) {
                        possibleMoves.push_back(positionToCheck);
                    }
                    break;
                }

                possibleMoves.push_back(positionToCheck);
            }
        }
    }


    return possibleMoves;
}