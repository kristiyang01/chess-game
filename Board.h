#ifndef BOARD_H
#define BOARD_H

#include "Square.h"
#include "PiecesContainer.h"

class Board
{
private:
    Square squares[8][8];  //TO DO BOARD_SIZE in stead of 8. Replace getMovementRange()'s 7 with BOARD_SIZE - 1. Make game function with any board size overall.
    PiecesContainer pieces;

public:
    Board();
    void setPieceToSquare(short row, short column, Piece* piece);
    void setSquare(short row, short column, Piece* piece, const Color& color);
    Square* getSquare(short row, short column);
    const Square& getSquare(short row, short column) const;  // TO DO check if I need both getSquare() bcz I dont rememberr why i need both
    Piece* getPieceAt(Position pos);
    void unhighlightSquares(std::vector<Position> possibleMoves, short possibleMovesCount);
    void highlightSquares(std::vector<Position> possibleMoves, short possibleMovesCount);
    void makeMove(Square* initialSquare, Square* targetSquare, short row, short column); // TO DO maybe this can be made with less parameters and will be better?
    std::vector<Position> getPossiblePieceMoves(Position attackingPiece); 

private:
    void initializeSquares();
    void initializePieces();
};

#endif

