#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <SFML/Graphics/Color.hpp> 

const double SQUARE_SIZE = 95.f; 
const double pieceSpriteWidthScale = 0.558;  //TO DO follow the same pattern when choosing const names
const double pieceSpriteHeightScale = 0.555;


//top tier
//const sf::Color customColorForWhite = sf::Color(249, 223, 223);
//const sf::Color customColorForBlack = sf::Color(185, 77, 139);

//probably not
//const sf::Color customColorForWhite = sf::Color(249, 223, 223);
//const sf::Color customColorForBlack = sf::Color(82, 20, 137);

//very good one
//const sf::Color customColorForWhite = sf::Color(235, 245, 255);
//const sf::Color customColorForBlack = sf::Color(94, 142, 196);

//very good one
//const sf::Color customColorForWhite = sf::Color(247, 235, 221);
//const sf::Color customColorForBlack = sf::Color(236, 127, 123);


const sf::Color customColorForWhite = sf::Color(247, 235, 221);
const sf::Color customColorForBlack = sf::Color(236, 127, 123);  
const sf::Color customColorForHighlightedWhite = sf::Color(247, 217, 197);
const sf::Color customColorForHighlightedBlack = sf::Color(218, 104, 100);



enum class Color
{
    WHITE,
    BLACK,
    NONE
};

#endif
