#include <iostream>
#include "Draw.h"

void Draw::drawBoard(const Board& board, sf::RenderWindow& window) {
	for (int row = 0; row < 8; row++) {
		for (int column = 0; column < 8; column++) {
			drawSquare(board.getSquare(row, column), window, row, column);
		}
	}
}

void Draw::drawSquare(const Square& square, sf::RenderWindow& window, short row, short column) {
	
	Color squareColor = square.getColor();
	sf::RectangleShape& squareSprite = sprites.getSquareSprite(squareColor, square.isHighlighted());
	squareSprite.setSize(sf::Vector2f(SQUARE_SIZE, SQUARE_SIZE));
	squareSprite.setPosition(column * SQUARE_SIZE, row * SQUARE_SIZE);
	window.draw(squareSprite);

	string pieceColorAndType = square.getPieceColorAndType();
	if (pieceColorAndType == "Null") {
		return;
	}
	sf::Sprite& pieceSprite = sprites.getPieceSprite(pieceColorAndType);

	pieceSprite.setPosition((column) * SQUARE_SIZE, (row) * SQUARE_SIZE);
	window.draw(pieceSprite);

	return;
}
