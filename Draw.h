#ifndef DRAW_H
#define DRAW_H


#include "Board.h"
#include <SFML/Graphics.hpp>
#include <unordered_map>
#include "SpriteManager.h"

#include <string>
using std::string;

class Draw {
private:
	SpriteManager sprites;

public:
	void drawBoard(const Board& board, sf::RenderWindow& window);

private:
	void drawSquare(const Square& square, sf::RenderWindow& window, short row, short column);
};




#endif