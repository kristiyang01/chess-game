#include <iostream>
#include <SFML/Graphics.hpp>
#include "Constants.h"

#include "Game.h"
#include "Board.h";
#include "Draw.h";
#include "MouseClickHandler.h"

#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"



void Game::run() {
    Board board;
    sf::RenderWindow window(sf::VideoMode(1000, 1000), "chess");
    Draw draw;
    MouseClickHandler mouseClickHandler;

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                    window.close();
                    break;

                case sf::Event::MouseButtonPressed: 
                    mouseClickHandler.reactToClick(event.mouseButton, board);
                    break;

                default:
                    std::cout << "default";
            };
        }
        window.clear();
        draw.drawBoard(board, window);
        window.display();
    }
}



    