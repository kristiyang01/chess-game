#include <iostream>
#include "King.h"

King::King(Position position, Color color, bool hasMoved, const string& type) : Piece(position, color, hasMoved, type) {}

std::vector<Position> King::getMovementDirection()
{

    std::vector<Position> movementDirection;

    movementDirection.push_back({ 0, 1 });
    movementDirection.push_back({ 0, -1 });
    movementDirection.push_back({ 1, 0 });
    movementDirection.push_back({ -1, 0 });
    movementDirection.push_back({ 1, 1 });
    movementDirection.push_back({ 1, -1 });
    movementDirection.push_back({ -1, 1 });
    movementDirection.push_back({ -1, -1 });

    return movementDirection;

}

short King::getMovementRange() {
    return 1;
}

std::vector<Position> King::getSpecialMovementDirection() {
    std::vector<Position> specialMovementDirection;

    specialMovementDirection.push_back({ 0, -1 });
    specialMovementDirection.push_back({ 0, 1  });

    return specialMovementDirection;
}

short King::getSpecialMovementRange() {
    return 2;
}