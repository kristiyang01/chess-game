#ifndef KING_H
#define KING_H

#include "Piece.h"

class King : public Piece
{

public:
    ~King() = default;
    King(Position position, Color color, bool hasMoved, const string& type);
    King() = default;

    std::vector<Position> getMovementDirection() override;
    short getMovementRange() override;
    std::vector<Position> getSpecialMovementDirection() override;
    short getSpecialMovementRange() override;

};

#endif