#include "Knight.h"

Knight::Knight(Position position, Color color, bool hasMoved, const string& type) : Piece(position, color, hasMoved, type) {}

std::vector<Position> Knight::getMovementDirection() {

    std::vector<Position> movementDirection;

    movementDirection.push_back({ 1, 2 });
    movementDirection.push_back({ 1, -2 });
    movementDirection.push_back({ -1, 2 });
    movementDirection.push_back({ -1, -2 });
    movementDirection.push_back({ 2, 1 });
    movementDirection.push_back({ 2, -1 });
    movementDirection.push_back({ -2, 1 });
    movementDirection.push_back({ -2, -1 });

    return movementDirection;

}

short Knight::getMovementRange() {
    return 1;
}

std::vector<Position> Knight::getSpecialMovementDirection() {
    return {};
}

short Knight::getSpecialMovementRange() {
    return 0;
}