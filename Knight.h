#ifndef KNIGHT_H
#define KNIGHT_H

#include "Piece.h"

class Knight : public Piece {
	
public:

	Knight(Position position, Color color, bool hasMoved, const string& type);
	~Knight() = default;

	std::vector<Position> getMovementDirection() override;
	short getMovementRange() override;
	std::vector<Position> getSpecialMovementDirection() override;
	short getSpecialMovementRange() override;

};






#endif
