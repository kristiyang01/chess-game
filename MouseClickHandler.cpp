#include "MouseClickHandler.h"

MouseClickHandler::MouseClickHandler()
    : clickedSquare(nullptr),
    initialSquare(nullptr),
    targetSquare(nullptr),
    pieceOnClickedSquare(nullptr),
    row(-1),
    column(-1),
    possibleMoves(),
    possibleMovesCount(0),
    whitePlayerTurn(true) {}

void MouseClickHandler::reactToClick(const sf::Event::MouseButtonEvent& mouseButton, Board& board) {
    row = mouseButton.y / SQUARE_SIZE;
    column = mouseButton.x / SQUARE_SIZE;

    if (clickIsOutOfBounds()) {
        return;
    }

    if (mouseButton.button == sf::Mouse::Left) {
        leftClick(board); //TO DO maybe in stead of leftclick i should choose a name that describes what left click does in chess
    }

    return;
}

bool MouseClickHandler::clickIsOutOfBounds() const {
    return row < 0 || row > 7 || column < 0 || row > 7;
}

bool MouseClickHandler::moveAllowed(Square* targetSquare) {
    return targetSquare->isHighlighted();
}

void MouseClickHandler::updatePossibleMoves(Board& board) {
    Position attackingPiecePosition = { row, column };
    possibleMoves = board.getPossiblePieceMoves(attackingPiecePosition);
}

void MouseClickHandler::updatePossibleMovesCount() {
    possibleMovesCount = possibleMoves.size();
}

void MouseClickHandler::leftClick(Board& board) {
    clickedSquare = board.getSquare(row, column);
    pieceOnClickedSquare = clickedSquare->getPiece();

    if (playerClickedOwnPiece()) {
        removePreviousHighlights(board);

        if (playerClickedSamePiece()) {
            initialSquare = nullptr;
            return;
        }
        
        initialSquare = clickedSquare;

        updatePossibleMoves(board);
        updatePossibleMovesCount();

        highlightPossibleMoves(board);
    }
    else {
        targetSquare = clickedSquare;
        tryMove(board, initialSquare, targetSquare);
        initialSquare = nullptr;

        removePreviousHighlights(board);
    }
}

void MouseClickHandler::removePreviousHighlights(Board& board) {
    board.unhighlightSquares(possibleMoves, possibleMovesCount);
}

void MouseClickHandler::highlightPossibleMoves(Board& board) {
    board.highlightSquares(possibleMoves, possibleMovesCount);
}

bool MouseClickHandler::playerClickedOwnPiece() {
    if (pieceOnClickedSquare == nullptr) {
        return false;
    }

    if (whitePlayerTurn) {
        return pieceOnClickedSquare->getColor() == Color::WHITE;
    }
    else {
        return pieceOnClickedSquare->getColor() == Color::BLACK;
    }
}

void MouseClickHandler::tryMove(Board& board, Square* initialSquare, Square* targetSquare) {
    if (initialSquare == nullptr || targetSquare == nullptr) {
        return;
    }

    if (moveAllowed(targetSquare)) {
        board.makeMove(initialSquare, targetSquare, row, column);
        whitePlayerTurn = !whitePlayerTurn;
    }
}

bool MouseClickHandler::playerClickedSamePiece() {
    return clickedSquare == initialSquare;
}