#ifndef MOUSECLICKHANDLER_H
#define MOUSECLICKHANDLER_H

#include "Board.h"
#include <SFML/Window/Event.hpp>

class MouseClickHandler {
private:
    Square* clickedSquare;
    Square* initialSquare;
    Square* targetSquare;
    Piece* pieceOnClickedSquare;
    short row;
    short column;
    std::vector<Position> possibleMoves;
    short possibleMovesCount;
    bool whitePlayerTurn;

public:
    MouseClickHandler();
    
    void reactToClick(const sf::Event::MouseButtonEvent& mouseButton, Board& board);
    bool moveAllowed(Square* targetSquare); //TO DO this should not be this class's responsibility 
       
private:
    bool clickIsOutOfBounds() const;
    void leftClick(Board& board);
    void highlightPossibleMoves(Board& board);
    void removePreviousHighlights(Board& board);
    bool playerClickedOwnPiece();
    void tryMove(Board& board, Square* initialSquare, Square* targetSquare);
    void updatePossibleMoves(Board& board);
    void updatePossibleMovesCount();
    bool playerClickedSamePiece();
};


#endif