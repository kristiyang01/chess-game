#include <iostream>
#include "Pawn.h"

Pawn::Pawn(Position position, Color color, bool hasMoved, const string& type) : Piece(position, color, hasMoved, type) {}

std::vector<Position> Pawn::getMovementDirection() {
	return {};
}

short Pawn::getMovementRange() {
	return 0;
}

std::vector<Position> Pawn::getSpecialMovementDirection() {
	std::vector<Position> moveDirection;

	if (getColor() == Color::WHITE) {
		moveDirection.push_back({ -1, 0 });
		moveDirection.push_back({ -1, 1 });
		moveDirection.push_back({ -1, -1 });
	}
	else
	{
		moveDirection.push_back({ 1, 0 });
		moveDirection.push_back({ 1, 1 });
		moveDirection.push_back({ 1, -1 });
	}

	return moveDirection;
}

short Pawn::getSpecialMovementRange() {
	return 1;
}