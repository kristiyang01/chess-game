#ifndef PAWN_H
#define PAWN_H

#include <vector>

#include "Piece.h"

class Pawn : public Piece
{

public:
    ~Pawn() = default;
    Pawn(Position position, Color color, bool hasMoved, const string& type);

    std::vector<Position> getMovementDirection() override;
    short getMovementRange() override;
    std::vector<Position> getSpecialMovementDirection() override;
    short getSpecialMovementRange() override;

};

#endif