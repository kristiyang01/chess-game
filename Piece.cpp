#include <iostream>
#include "Piece.h"

using namespace std;

Piece::Piece() {
    this->position.row = 0; //TODO all of the below maybe
    this->position.column = 0;
    this->color = Color::NONE;
    this->hasMoved = false;
    type = "Null";
}

Piece::Piece(Position position, Color color, bool hasMoved, const string& type)
{
    this->position.row = position.row;
    this->position.column = position.column;
    this->color = color;
    this->hasMoved = hasMoved;
    setType(type);
}

void Piece::setPosition(short row, short column)
{
    this->position.row = row;
    this->position.column = column;

    return;
}

void Piece::setColor(Color color)
{
    this->color = color;

    return;
}

void Piece::setType(string type) {   //TO DO maybe this is easily avoided by using ENUM for the type.
    short asciiOffset = 'z' - 'Z';
    if (type[0] <= 'z' && type[0] >= 'a') {
        type[0] = type[0] - asciiOffset;
    }

    short typeLength = type.size();

    for (int i = 1; i < typeLength; i++) {
        if (type[i] <= 'Z' && type[i] >= 'A') {
            type[i] = type[i] + asciiOffset;
        }
    }

    this->type = type;
}

void Piece::setHasMoved(bool hasMoved) {
    this->hasMoved = hasMoved;
}

short Piece::getPositionRow() const
{
    return position.row;
}

short Piece::getPositionColumn() const
{
    return position.column;
}

Color Piece::getColor() const
{
    return color;
}

string Piece::getType() const {
    return type;
}

string Piece::getColorAndTypeString() const {
    
    if (type == "Null") {
        return type;
    }

    string colorString;
    if (color == Color::WHITE) {
        colorString = "white";
    }
    else {
        colorString = "black";
    }
    return colorString + type;
}

bool Piece::hasBeenMoved() const
{
    return hasMoved;
}

