#ifndef PIECE_H
#define PIECE_H

#include <vector>
#include "Constants.h"
#include <string>
using std::string;

struct Position //TO DO extract in class and add operator+ maybe
{
    short row;
    short column;

    bool isValidPosition() const
    {
        if (row >= 0 && row <= 7 && column >= 0 && column <= 7)
        {
            return true;
        }

        return false;
    }

};

class Piece
{
private:
    Position position;
    Color color;
    bool hasMoved;
    string type;

public:
    Piece();
    virtual ~Piece() = default;
    Piece(Position position, Color color, bool hasMoved, const string& type);

    void setPosition(short row, short column);
    void setColor(Color color);
    void setType(string type);
    void setHasMoved(bool hasMoved);

    short getPositionRow() const;
    short getPositionColumn() const;
    virtual std::vector<Position> getMovementDirection() = 0;
    virtual short getMovementRange() = 0;
    virtual std::vector<Position> getSpecialMovementDirection() = 0;
    virtual short getSpecialMovementRange() = 0;
    Color getColor() const;
    string getType() const;
    string getColorAndTypeString() const;
    bool hasBeenMoved() const;
};

#endif