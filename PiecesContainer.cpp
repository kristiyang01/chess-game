#include "PiecesContainer.h"
#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"


PiecesContainer::PiecesContainer() {

    Position initialPiecePosition;
    Color color;
    bool hasMoved = false;
    std::string type;

  
    color = Color::WHITE;

    type = "Pawn";
    
    for (short i = 0; i < 8; i++) {
        initialPiecePosition = { 6, i };
        pieces[i] = std::make_unique<Pawn>(initialPiecePosition, color, hasMoved, type);
    }


    type = "Knight";

    initialPiecePosition = { 7, 1 };    
    pieces[8] = std::make_unique<Knight>(initialPiecePosition, color, hasMoved, type);
    initialPiecePosition = { 7, 6 };
    pieces[9] = std::make_unique<Knight>(initialPiecePosition, color, hasMoved, type);


    type = "Bishop";

    initialPiecePosition = { 7, 2 };
    pieces[10] = std::make_unique<Bishop>(initialPiecePosition, color, hasMoved, type);
    initialPiecePosition = { 7, 5 };
    pieces[11] = std::make_unique<Bishop>(initialPiecePosition, color, hasMoved, type);


    type = "Rook";

    initialPiecePosition = { 7, 0 };
    pieces[12] = std::make_unique<Rook>(initialPiecePosition, color, hasMoved, type);
    initialPiecePosition = { 7, 7 };
    pieces[13] = std::make_unique<Rook>(initialPiecePosition, color, hasMoved, type);


    type = "Queen";

    initialPiecePosition = { 7, 3 };
    pieces[14] = std::make_unique<Queen>(initialPiecePosition, color, hasMoved, type);


    type = "King";

    initialPiecePosition = { 7, 4 };
    pieces[15] = std::make_unique<King>(initialPiecePosition, color, hasMoved, type);


    
    color = Color::BLACK;


    type = "Pawn";

    for (short i = 0; i < 8; i++) {
        initialPiecePosition = { 1, i };
        pieces[16+i] = std::make_unique<Pawn>(initialPiecePosition, color, hasMoved, type);
    }


    type = "Knight";

    initialPiecePosition = { 0, 1 };
    pieces[24] = std::make_unique<Knight>(initialPiecePosition, color, hasMoved, type);
    initialPiecePosition = { 0, 6 };
    pieces[25] = std::make_unique<Knight>(initialPiecePosition, color, hasMoved, type);


    type = "Bishop";

    initialPiecePosition = { 0, 2 };
    pieces[26] = std::make_unique<Bishop>(initialPiecePosition, color, hasMoved, type);
    initialPiecePosition = { 0, 5 };
    pieces[27] = std::make_unique<Bishop>(initialPiecePosition, color, hasMoved, type);


    type = "Rook";

    initialPiecePosition = { 0, 0 };
    pieces[28] = std::make_unique<Rook>(initialPiecePosition, color, hasMoved, type);
    initialPiecePosition = { 0, 7 };
    pieces[29] = std::make_unique<Rook>(initialPiecePosition, color, hasMoved, type);


    type = "Queen";

    initialPiecePosition = { 0, 3 };
    pieces[30] = std::make_unique<Queen>(initialPiecePosition, color, hasMoved, type);


    type = "King";

    initialPiecePosition = { 0, 4 };
    pieces[31] = std::make_unique<King>(initialPiecePosition, color, hasMoved, type);

}


Piece* PiecesContainer::operator[](short index) {
    return pieces[index].get();
}

