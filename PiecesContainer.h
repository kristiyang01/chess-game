#ifndef PIECESCONTAINER_H
#define PIECESCONTAINER_H


#include "memory"

#include "Piece.h"
#include "King.h"

class PiecesContainer {
private:
	std::unique_ptr<Piece> pieces[32];

public:
	PiecesContainer();
	Piece* operator[](short index);


    
};







#endif