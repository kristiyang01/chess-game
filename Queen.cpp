#include <iostream>
#include "Queen.h"

Queen::Queen(Position position, Color color, bool hasMoved, const string& type) : Piece(position, color, hasMoved, type) {}

std::vector<Position> Queen::getMovementDirection()
{

    std::vector<Position> movementDirection;

    movementDirection.push_back({ 0, 1 });
    movementDirection.push_back({ 0, -1 });
    movementDirection.push_back({ 1, 0 });
    movementDirection.push_back({ -1, 0 });
    movementDirection.push_back({ 1, 1 });
    movementDirection.push_back({ 1, -1 });
    movementDirection.push_back({ -1, 1 });
    movementDirection.push_back({ -1, -1 });

    return movementDirection;

}

short Queen::getMovementRange() {
    return 7;
}

std::vector<Position> Queen::getSpecialMovementDirection() {
    return {};
}

short Queen::getSpecialMovementRange() {
    return 0;
}