#ifndef QUEEN_H
#define QUEEN_H

#include "Piece.h"

class Queen : public Piece
{

public:
    ~Queen() = default;
    Queen(Position position, Color color, bool hasMoved, const string& type);

    std::vector<Position> getMovementDirection() override;
    short getMovementRange() override;
    std::vector<Position> getSpecialMovementDirection() override;
    short getSpecialMovementRange() override;

};

#endif