# Chess-game


This is an implementation of Chess with the idea of practising some clean code principles I learnt and achieving such architecture that would allow me to easily add more rules, pieces and other features. 

The game is almost finished. Currently I am refactoring and thinking of a way to make adding many rules easy after which, I will finish the application by add enpassant, castling and a few more rules + checks if player won the game
