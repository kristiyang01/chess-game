#include "Rook.h"


Rook::Rook(Position position, Color color, bool hasMoved, const string& type) : Piece(position, color, hasMoved, type) {}

std::vector<Position> Rook::getMovementDirection() {

    std::vector<Position> movementDirection;

    movementDirection.push_back({ 0, 1 });
    movementDirection.push_back({ 0, -1 });
    movementDirection.push_back({ 1, 0 });
    movementDirection.push_back({ -1, 0 });

    return movementDirection;

}


short Rook::getMovementRange() {
    return 7;
}

std::vector<Position> Rook::getSpecialMovementDirection() {
    return {};
}

short Rook::getSpecialMovementRange() {
    return 0;
}