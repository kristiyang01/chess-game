#ifndef ROOK_H
#define ROOK_H

#include "Piece.h"

class Rook : public Piece
{

public:
    ~Rook() = default;
    Rook(Position position, Color color, bool hasMoved, const string& type);
    std::vector<Position> getMovementDirection() override;
    short getMovementRange() override;
    std::vector<Position> getSpecialMovementDirection() override;
    short getSpecialMovementRange() override;

};

#endif