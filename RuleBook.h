#ifndef RULEBOOK_H
#define RULEBOOK_H

#include "Board.h"

class RuleBook {
private:

public:
	//void RuleBook::fillPossibleMoves(Board& board, Position attackingPiecePosition, std::vector<Position>& possibleMoves) {
	//	Piece* attackingPiece = board.getPieceAt(attackingPiecePosition);

	//	std::vector<Position> movementDirections = attackingPiece->getMovementDirection();
	//	int movementDirectionsCount = movementDirections.size();
	//	int pieceMovementRange = attackingPiece->getMovementRange();

	//	Position positionToCheck = attackingPiecePosition;

	//	for (int i = 0; i < movementDirectionsCount; i++) {
	//		positionToCheck = attackingPiecePosition;

	//		for (int j = 0; j < pieceMovementRange; j++) {

	//			positionToCheck.row += movementDirections[i].row;
	//			positionToCheck.column += movementDirections[i].column;

	//			if (!positionToCheck.isValidPosition()) {
	//				break;
	//			}

	//			Square* squareToCheck = board.getSquare(positionToCheck.row, positionToCheck.column); //TO DO this should maybe be a shared pointer?

	//			if (squareToCheck->hasPiece()) {
	//				if (squareToCheck->getColor() != attackingPiece->getColor()) {
	//					possibleMoves.push_back(positionToCheck);
	//				}
	//				break;
	//			}

	//			possibleMoves.push_back(positionToCheck);

	//		}
	//	}
	//}

	// uncomment below and continue it next time im working on the project (DO)
	/*void RuleBook::fillPossibleMoves(Board& board, Position attackingPiecePosition, std::vector<Position>& possibleMoves) {
		Piece* attackingPiece = board.getPieceAt(attackingPiecePosition);

		std::vector<Position> normalMovementDirections = attackingPiece->getMovementDirection();
		int normalMovementRange = attackingPiece->getNormalMovementRange();
		fillWithAllowedMoves(possibleMoves, board, attackingPiecePosition, normalMovementDirections, normalMovementRange);


		std::vector<Position> specialMovementDirection = attackingPiece->getSpecialMovementDirection();
		int specialMovementRange = attackingPiece->getSpecialMovementRange();
		fillWithAllowedMoves(possibleMoves, board, attackingPiecePosition, specialMovementDirection, specialMovementRange);

	}

	void RuleBook::fillWithAllowedMoves(const Board& board, Position attackingPiecePosition, std::vector<Position>& possibleMoves, std::vector<Position> movementDirections, int movementRange) {

		int movementDirectionsCount = specialMovementDirections.size();

		for (int i = 0; i < count; i++) {
			positionToCheck = attackingPiecePosition;

			for (int j = 0; j < specialMovementRange; j++) {
				positionToCheck += specialMovementDirections[i];

				if (moveMeetsRequirements(positionToCheck)) {
					possibleMoves.push_back(positionToCheck);
				}
			}
		}
	}


	std::vector<Position> RuleBook::getPossiblePieceMoves(Board& board, Position attackingPiecePosition) {
		std::vector<Position> possibleMoves; //TO DO maybe think if stack is good here?

		fillPossibleMoves(board, attackingPiecePosition, possibleMoves);

		return possibleMoves;
	}

	bool RuleBook::moveMeetsRequirements(Piece* attackingPiece) {
		
		if (!positionToCheck.isValidPosition()) {
			return false;
		}


		string attackingPieceType = attackingPiece->getType();

		switch (attackingPieceType) {
		case "Pawn":
			return pawnSideWayMoveAllowed() || pawnForwardMoveAllowed();

		case "King":
			return normalMoveRequirements() || shortCastleAllowed() || longCastleAllowed();

		case default:
			return normalMoveRequirements();
		}
	}

	bool RuleBook::normalMoveRequirements(Piece* attackingPiece, Position positionToCheck) {
		
		Square* squareToCheck = getSquare(positionToCheck.row, positionToCheck.column); //TO DO this should maybe be a shared pointer?

		if (squareToCheck->hasPiece()) {
			if (attackingPiece->getColor() != squareToCheck->getPieceColor()) {
				return true;
			}
			return false;
		}

		return true;
	}

	bool RuleBook::pawnForwardMoveRequirementsMet() {
		if(positionToCheck.column == attackingPiecePosition.column) {
			if (!squareToCheck->hasPiece()) {
				return true;


			}

		
		if (i == 0) {
				if (!squareToCheck->hasPiece()) {
					possibleMoves.push_back(positionToCheck);

					if (!attackingPiece->hasBeenMoved()) {
						positionToCheck.row += movementDirections[i].row;
						squareToCheck = getSquare(positionToCheck.row, positionToCheck.column);
						if (!squareToCheck->hasPiece()) {
							possibleMoves.push_back(positionToCheck); //TO DO REFACTOR THIS FOR SURE!!!!!!!
						}
					}
				}
			}
			else {
				if (squareToCheck->hasPiece() && squareToCheck->getPieceColor() != attackingPiece->getColor()) { //TO DO  || lastMoveLeadsToEnpassant  and maybe help function
					possibleMoves.push_back(positionToCheck);
				}
			}
		}
	}








	for (int i = 0; i < size; i++) {
		positionToCheck = attackingPiecePosition;

		for (int j = 0; j < pieceMovementRange; j++) {

			positionToCheck.row += movementDirections[i].row;
			positionToCheck.column += movementDirections[i].column;
			if (!positionToCheck.isValidPosition()) {
				break;
			}

			Square* squareToCheck = getSquare(positionToCheck.row, positionToCheck.column); //TO DO this should maybe be a shared pointer?

			if (squareToCheck->hasPiece()) {
				if (attackingSquare->getPieceColor() != squareToCheck->getPieceColor()) {
					possibleMoves.push_back(positionToCheck);
				}
				break;
			}

			possibleMoves.push_back(positionToCheck);
		}
	}



	std::vector<Position> Board::getPossiblePieceMoves(Position attackingPiecePosition) {  //TO DO refactor this for sure its too long
		std::vector<Position> possibleMoves; //TO DO maybe think if stack is good here?

		Square* attackingSquare = getSquare(attackingPiecePosition.row, attackingPiecePosition.column);
		Piece* attackingPiece = attackingSquare->getPiece();

		short pieceMovementRange = attackingSquare->getPieceMovementRange();
		std::vector<Position> movementDirections = attackingSquare->getPieceMovementDirection();
		short size = movementDirections.size();

		Position positionToCheck;

		if (attackingPiece->getType() == "Pawn") {

			for (int i = 0; i < size; i++) {
				positionToCheck.row = attackingPiecePosition.row + movementDirections[i].row;
				positionToCheck.column = attackingPiecePosition.column + movementDirections[i].column;
				Square* squareToCheck = getSquare(positionToCheck.row, positionToCheck.column);

				if (i == 0) {
					if (!squareToCheck->hasPiece()) {
						possibleMoves.push_back(positionToCheck);

						if (!attackingPiece->hasBeenMoved()) {
							positionToCheck.row += movementDirections[i].row;
							squareToCheck = getSquare(positionToCheck.row, positionToCheck.column);
							if (!squareToCheck->hasPiece()) {
								possibleMoves.push_back(positionToCheck); //TO DO REFACTOR THIS FOR SURE!!!!!!!
							}
						}
					}
				}
				else {
					if (squareToCheck->hasPiece() && squareToCheck->getPieceColor() != attackingPiece->getColor()) { //TO DO  || lastMoveLeadsToEnpassant  and maybe help function
						possibleMoves.push_back(positionToCheck);
					}
				}
			}
		}
		else {
			if (attackingPiece->getType() == "King") {
				if (leftSideCastleAllowed()) {
					specialMovementDirection = attackingSquare->getPieceSpecialMovementDirection();
					specialMovementRange = specialMovementDirection.size();
				}
			}

			for (int i = 0; i < size; i++) {
				positionToCheck = attackingPiecePosition;

				for (int j = 0; j < pieceMovementRange; j++) {

					positionToCheck.row += movementDirections[i].row;
					positionToCheck.column += movementDirections[i].column;
					if (!positionToCheck.isValidPosition()) {
						break;
					}

					Square* squareToCheck = getSquare(positionToCheck.row, positionToCheck.column); //TO DO this should maybe be a shared pointer?

					if (squareToCheck->hasPiece()) {
						if (attackingSquare->getPieceColor() != squareToCheck->getPieceColor()) {
							possibleMoves.push_back(positionToCheck);
						}
						break;
					}

					possibleMoves.push_back(positionToCheck);
				}
			}
		}


		return possibleMoves;
	}

	*/

public:

	//DELETE UNDER THIS STATEMENT (DO)

	/*void funcMove(Position from, Position to, Board& board) {
		Square* initialSquare = board[from.row][from.column];
		Square* destinationSquare = board[to.row][to.column];

		short pieceMovementRange = initialSquare->getPieceMovementRange();
		std::vector<Position> movementDirections = initialSquare->getPieceMovementDirection();
		short size = movementDirections.size();

		Position positionToCheck;
		for (int i = 0; i < size; i++) {

			positionToCheck = from;

			for (int j = 0; j < pieceMovementRange; j++) {

				positionToCheck.row += movementDirections[i].row;
				positionToCheck.column += movementDirections[i].column;
				if (!positionToCheck.isValidPosition()) {
					break;
				}

				Square* squareToCheck = board.getSquare(positionToCheck.row, positionToCheck.column);

				if (squareToCheck->hasPiece()) {
					if (initialSquare->getColor() != squareToCheck->getColor()) {
						//highlight
					}
					break;
				}

				//highlight





			}

		}

	}





	bool moveAllowed(short rowFrom, short columnFrom, short rowTo, short columnTo) {

		if (rowFrom == rowTo) {
			if (columnFrom == columnTo) {
				return false;
			}


		}




		for (int direction = 0; direction < directionsCount; direction++) {
			for (int j = 0; j < jSize; j++) {

				if (board[offset1][offset2].hasPiece) {
					break;
				}
			}
		}
	}
	*/


	//castling
	//en passant
	//promotion
	//pawn side taking
	//pin
	//check
	//checkmate
	//draw
	//king cant go next to king
};


#endif
