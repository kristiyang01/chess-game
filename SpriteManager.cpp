#include "SpriteManager.h"


SpriteManager::SpriteManager() {
	setSquareSize();
	loadTextures();
	setSprites();
}

sf::Sprite& SpriteManager::getPieceSprite(const string& pieceColorAndType) {
	return pieceSprites.at(pieceColorAndType);
}

sf::RectangleShape& SpriteManager::getSquareSprite(const Color& color, bool isHighlighted) {
	if (color == Color::WHITE) {
		if (isHighlighted) {
			square.setFillColor(customColorForHighlightedWhite);
		}
		else {
			square.setFillColor(customColorForWhite);
		}
	}
	else {
		if (isHighlighted) {
			square.setFillColor(customColorForHighlightedBlack);
		}
		else {
			square.setFillColor(customColorForBlack);
		}
	}
	return square;
}

void SpriteManager::setSquareSize() {
	square = sf::RectangleShape(sf::Vector2f(SQUARE_SIZE, SQUARE_SIZE));
}

void SpriteManager::loadTextures() {
	if (!pieceTextures[0].loadFromFile("assets/pieces/w_king.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/w_king.png");
	}
	
	if (!pieceTextures[1].loadFromFile("assets/pieces/w_queen.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/w_queen.png");
	}
	
	if (!pieceTextures[2].loadFromFile("assets/pieces/w_rook.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/w_rook.png");
	}

	if (!pieceTextures[3].loadFromFile("assets/pieces/w_bishop.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/w_bishop.png");
	}

	if (!pieceTextures[4].loadFromFile("assets/pieces/w_knight.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/w_knight.png");
	}

	if (!pieceTextures[5].loadFromFile("assets/pieces/w_pawn.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/w_pawn.png");
	}

	if (!pieceTextures[6].loadFromFile("assets/pieces/b_king.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/b_king.png");
	}

	if (!pieceTextures[7].loadFromFile("assets/pieces/b_queen.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/b_queen.png");
	}

	if (!pieceTextures[8].loadFromFile("assets/pieces/b_rook.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/b_rook.png");
	}

	if (!pieceTextures[9].loadFromFile("assets/pieces/b_bishop.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/b_bishop.png");
	}

	if (!pieceTextures[10].loadFromFile("assets/pieces/b_knight.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/b_knight.png");
	}

	if (!pieceTextures[11].loadFromFile("assets/pieces/b_pawn.png")) {
		throw std::runtime_error("Failed to load texture: assets/pieces/b_pawn.png");
	}
}

void SpriteManager::setSprites() {
	sf::Sprite pieceSprite;
	pieceSprite.setScale(sf::Vector2f(pieceSpriteWidthScale, pieceSpriteHeightScale)); //TO DO maybe scaling the image shouldnt be to this class 
	                                                                                   //and maybe this class needs different name?
	pieceSprite.setTexture(pieceTextures[0]);
	pieceSprites["whiteKing"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[1]);
	pieceSprites["whiteQueen"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[2]);
	pieceSprites["whiteRook"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[3]);
	pieceSprites["whiteBishop"] = pieceSprite;
	
	pieceSprite.setTexture(pieceTextures[4]);
	pieceSprites["whiteKnight"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[5]);
	pieceSprites["whitePawn"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[6]);
	pieceSprites["blackKing"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[7]);
	pieceSprites["blackQueen"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[8]);
	pieceSprites["blackRook"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[9]);
	pieceSprites["blackBishop"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[10]);
	pieceSprites["blackKnight"] = pieceSprite;

	pieceSprite.setTexture(pieceTextures[11]);
	pieceSprites["blackPawn"] = pieceSprite;
}
