#ifndef SPRITEMANAGER_H
#define SPRITEMANAGER_H

#include "Constants.h"
#include <SFML/Graphics.hpp>
#include <unordered_map>
#include <string>
using std::string;


class SpriteManager {
private:
	sf::Texture pieceTextures[12];
	std::unordered_map<string, sf::Sprite> pieceSprites;
	sf::RectangleShape square;

public:
	SpriteManager();
	sf::Sprite& getPieceSprite(const string& pieceColorAndType);
	sf::RectangleShape& getSquareSprite(const Color& color, bool isHighlighted);
	
private:
	void setSquareSize();
	void loadTextures();
	void setSprites();
};

#endif