#include <iostream>
#include "Square.h"

using namespace std;

Square::Square(Piece* piece, Color color, bool highlighted)
{
    this->piece = piece;
    this->color = color;
    this->highlighted = highlighted;
}

Square::Square() {
    this->piece = nullptr;
    this->color = Color::NONE;
    this->highlighted = false;
}

void Square::setColor(Color color)
{
    this->color = color;
}

void Square::setPiece(Piece* piece)
{
    this->piece = piece;
}

void Square::setHighlighted(bool highlighted) {
    this->highlighted = highlighted;
}

Color Square::getColor() const {
    return color;
}

Color Square::getPieceColor() const {
    if (piece == nullptr) {
        return Color::NONE;
    }

    return piece->getColor();
}

string Square::getPieceType() const {
    if (piece == nullptr) {
        return "Null";
    }

    return piece->getType();
}

string Square::getPieceColorAndType() const {
    if (piece == nullptr) {
        return "Null";
    }

    return piece->getColorAndTypeString();
}

std::vector<Position> Square::getPieceMovementDirection() {
    return this->piece->getMovementDirection();  //TO DO dont I copy assign twice to get the final vector which is slow
}

short Square::getPieceMovementRange() {
    return this->piece->getMovementRange();
}

Piece* Square::getPiece() {
    return this->piece;
}

bool Square::isHighlighted() const {
    return this->highlighted;
}

bool Square::hasPiece() const {
    return this->piece != nullptr;
}

