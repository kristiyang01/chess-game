#ifndef SQUARE_H
#define SQUARE_H

#include "Piece.h"
#include "Constants.h"

class Square
{
private:
    Piece* piece;
    Color color;
    bool highlighted;

public:
    Square();
    Square(Piece* piece, Color color, bool isHighlighted);
    ~Square() = default;

    void setColor(Color color);
    void setPiece(Piece* piece);
    void setHighlighted(bool highlighted);
    
    Color getColor() const;
    Color getPieceColor() const;
    string getPieceType() const;
    string getPieceColorAndType() const;
    std::vector<Position> getPieceMovementDirection();
    short getPieceMovementRange();
    Piece* getPiece();
    bool isHighlighted() const;
    bool hasPiece() const;
};

#endif