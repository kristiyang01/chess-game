#include <iostream>
using namespace std;

#include <SFML/Graphics.hpp>

using namespace std;

const double SQUARE_SIZE = 90.f;
const sf::Color customColorForWhite = sf::Color(233, 179, 195);
const sf::Color customColorForBlack = sf::Color(185, 189, 219);

int main()
{

    sf::RenderWindow window(sf::VideoMode(1000, 1000), "chess");
    
    

    sf::RectangleShape squares[64];

    for (int i = 0; i < 64; i++) {
        squares[i].setSize(sf::Vector2f(SQUARE_SIZE, SQUARE_SIZE));
        
        int row = i / 8;
        int column = i % 8;

        squares[i].setPosition(column * SQUARE_SIZE, row * SQUARE_SIZE);
        
        if (row % 2 == column % 2) {
            squares[i].setFillColor(customColorForWhite);
        }
        else {
            squares[i].setFillColor(customColorForBlack);
        }
    }



    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        } 

        window.clear();

        for (int i = 0; i < 64; i++) {
            window.draw(squares[i]);
        }

        window.display();
    }

    return 0;
}






------------------------------------------



#include <iostream>
using namespace std;

#include <SFML/Graphics.hpp>

using namespace std;

const double SQUARE_SIZE = 90.f;
const sf::Color customColorForWhite = sf::Color(233, 179, 195);
const sf::Color customColorForBlack = sf::Color(185, 189, 219);

int main()
{

    sf::RenderWindow window(sf::VideoMode(1000, 1000), "chess");
    

    sf::RectangleShape square(sf::Vector2f(SQUARE_SIZE, SQUARE_SIZE));

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        } 

        window.clear();

        for (int i = 0; i < 64; i++) {
            
            int row = i / 8;
            int column = i % 8;

            square.setPosition(column * SQUARE_SIZE, row * SQUARE_SIZE);

            if (row % 2 == column % 2) {
                square.setFillColor(customColorForWhite);
            }
            else {
                square.setFillColor(customColorForBlack);
            }

            window.draw(square);
        }
      
        window.display();
    }

    return 0;
}
